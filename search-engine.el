;;; search-engine.el --- Query search engines -*- lexical-binding: t -*-


;; This file is part of emacs-search-engine.

;; emacs-search-engine is free software: you can redistribute it and/or modify
;; it under the terms of the GNU General Public License as published by
;; the Free Software Foundation, version 3.

;; emacs-search-engine is distributed in the hope that it will be useful,
;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;; GNU General Public License for more details.

;; You should have received a copy of the GNU General Public License
;; along with emacs-search-engine.  If not, see <https://www.gnu.org/licenses/>.

;; Copyright (c) 2022, Maciej Barć <xgqt@riseup.net>
;; Licensed under the GNU GPL v3 License


;; Author: Maciej Barć <xgqt@riseup.net>
;; Homepage: https://gitlab.com/xgqt/emacs-search-engine/
;; Version: 0.1.0
;; Keywords: convenience hypermedia
;; Package-Requires: ((emacs "24.4"))



;;; Commentary:


;; Query search engines from Emacs.

;; The search-engine package allows You to query predefined search engines
;; (‘search-engine-custom-engines’) with interactive selection.
;; The query terms can either be extracted form selection, kill-ring
;; or typed on demand.

;; The `search-engine' function is a interactive entry-point to select both
;; the terms extraction method and search engine provider.

;; To turn on the global mode enabling a custom key map,
;; activate `search-engine-mode'.

;; ‘search-engine’ is inspired by ‘engine-mode’
;; (https://github.com/hrs/engine-mode), but the differences are big enough
;; for it to be it's own package.



;;; Code:


(require 'search-engine-custom)


(defgroup search-engine nil
  "Query search engines from Emacs."
  :group 'convenience
  :group 'external
  :group 'hypermedia
  :group 'web)


(defconst search-engine-version "0.1.0"
  "Search-Engine package version.")

(defconst search-engine-methods
  '(("point"     . search-engine-point)
    ("region"    . search-engine-region)
    ("kill-ring" . search-engine-kill-ring)
    ("term"      . search-engine-term))
  "Methods of `search-engine'.

Each element is an association pair composed of a method name and a function
that is defined in Search-Engine package.")


(defun search-engine--methods-names ()
  "Return the names of ‘search-engine-methods’."
  (mapcar #'car search-engine-methods))

(defun search-engine--method-value (method-name)
  "Return value of METHOD-NAME from ‘search-engine-methods’."
  (cdr (assoc method-name search-engine-methods)))

(defun search-engine--engine-names ()
  "Return the names of ‘search-engine-engines’."
  (mapcar #'car search-engine-custom-engines))

(defun search-engine--engine-properties (engine-name)
  "Return engine properties associated with ENGINE-NAME."
  (let ((properties-list
         (assoc engine-name search-engine-custom-engines)))
    `((name      . ,(nth 0 properties-list))
      (separator . ,(nth 1 properties-list))
      (query-url . ,(nth 2 properties-list)))))

(defun search-engine--property-value (property properties)
  "Return value of PROPERTY from PROPERTIES."
  (cdr (assoc property properties)))

(defun search-engine--select-engine ()
  "Return the query URL.

URL is extracted from associated with search engine
selected from completing read."
  (let* ((engine-names
          (search-engine--engine-names))
         (selected-engine
          (completing-read "Search engine: "
                           engine-names
                           nil
                           t
                           search-engine-custom-default-engine)))
    (search-engine--engine-properties selected-engine)))

(defun search-engine--form-query (query-url separator words-string)
  "Form a full search URL query.

Returns URL formed from formatted QUERY-URL, SEPARATOR and WORDS-STRING."
  (concat "https://"
          query-url
          (url-hexify-string (replace-regexp-in-string " "
                                                       (string separator)
                                                       words-string))))

(defun search-engine--browse-url (words-string)
  "Browse the full query URL.

WORDS-STRING is the search term for a search engine selected interactively
by the user."
  (let* ((engine-properties
          (search-engine--select-engine))
         (query-url
          (search-engine--property-value 'query-url engine-properties))
         (separator
          (search-engine--property-value 'separator engine-properties)))
    (funcall search-engine-custom-browse-url-function
             (search-engine--form-query query-url separator words-string))))


;;;###autoload
(defun search-engine-browse-with (browse-url-function)
  "Set the function used to browse full query URLs to BROWSE-URL-FUNCTION."
  (interactive
   (list (intern (completing-read
                  "Browse with function: "
                  search-engine-custom-browse-url-function-candidates))))
  (message "Selected function: %s" browse-url-function)
  (setq-default search-engine-custom-browse-url-function browse-url-function))

;;;###autoload
(defun search-engine-point ()
  "Query search engines based on `thing-at-point'."
  (interactive)
  (search-engine--browse-url (thing-at-point 'symbol 'no-properties)))

;;;###autoload
(defun search-engine-region (start end)
  "Query search engines based on selected buffer region.

START and END come from the selected region, they form the search term."
  (interactive)
  (let ((words-string
         (buffer-substring start end)))
    (search-engine--browse-url words-string)))

;;;###autoload
(defun search-engine-kill-ring ()
  "Query search engines based on ‘kill-ring’."
  (interactive)
  (let* ((kill-ring-contents
          (mapcar #'substring-no-properties kill-ring))
         (words-string
          (completing-read "Term from kill-ring: " kill-ring-contents)))
    (search-engine--browse-url words-string)))

;;;###autoload
(defun search-engine-term (words-string)
  "Query search engines based on prompt.

WORDS-STRING is the search term."
  (interactive "sSearch term: ")
  (search-engine--browse-url words-string))

;;;###autoload
(defun search-engine (method-name)
  "Query search engines with a METHOD-NAME.

The list of possible selections is defined by ‘search-engine-methods’."
  (interactive
   (let* ((method-names
           (search-engine--methods-names))
          (method-name
           (completing-read "Search method: " method-names nil t)))
     (list method-name)))
  (call-interactively (search-engine--method-value method-name)))


(provide 'search-engine)



;;; search-engine.el ends here
