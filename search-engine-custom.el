;;; search-engine-custom.el --- Customization for search-engine -*- lexical-binding: t -*-


;; This file is part of emacs-search-engine.

;; emacs-search-engine is free software: you can redistribute it and/or modify
;; it under the terms of the GNU General Public License as published by
;; the Free Software Foundation, version 3.

;; emacs-search-engine is distributed in the hope that it will be useful,
;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;; GNU General Public License for more details.

;; You should have received a copy of the GNU General Public License
;; along with emacs-search-engine.  If not, see <https://www.gnu.org/licenses/>.

;; Copyright (c) 2022, Maciej Barć <xgqt@riseup.net>
;; Licensed under the GNU GPL v3 License



;;; Commentary:


;; Customization for ‘search-engine’.



;;; Code:


(defcustom search-engine-custom-engines
  '(("c++-docs"         ?+  "cplusplus.com/search.do?q=")
    ("codeberg"         ?+  "codeberg.org/explore/repos?q=")
    ("die"              ?+  "www.die.net/search/?q=")
    ("django-docs"      ?+  "docs.djangoproject.com/en/4.0/search/?q=")
    ("duckduckgo"       ?+  "duckduckgo.com/?q=")
    ("gentoo-bugs"      ?+  "bugs.gentoo.org/buglist.cgi?quicksearch=")
    ("gentoo-overlays"  ?+  "gpo.zugaina.org/Search?search=")
    ("gentoo-packages"  ?+  "packages.gentoo.org/packages/search?q=")
    ("gentoo-wiki"      ?+  "wiki.gentoo.org/index.php?search=")
    ("github"           ?+  "github.com/search?q=")
    ("gitlab"           ?+  "gitlab.com/search?search=")
    ("google"           ?+  "google.com/search?q=")
    ("google-maps"      ?+  "maps.google.com/maps?q=")
    ("julia-docs"       ?+  "docs.julialang.org/en/v1/search/?q=")
    ("julia-packages"   ?+  "juliapackages.com/packages?search=")
    ("melpa"            ?+  "melpa.org/#/?q=")
    ("melpa-stable"     ?+  "stable.melpa.org/#/?q=")
    ("odysee"           ?+  "odysee.com/$/search?q=")
    ("peertube"         ?+  "search.joinpeertube.org/search?search=")
    ("python-docs"      ?+  "docs.python.org/3/search.html?q=")
    ("python-packages"  ?+  "pypi.org/search/?q=")
    ("qwant"            ?+  "qwant.com/?q=")
    ("sjp"              ?\s "sjp.pwn.pl/slowniki/")  ; Polish dictionary
    ("racket-docs"      ?\s "docs.racket-lang.org/search/index.html?q=")
    ("racket-packages"  ?+  "pkgd.racket-lang.org/pkgn/search?q=")
    ("reddit"           ?+  "reddit.com/search/?q=")
    ("repology"         ?-  "repology.org/projects/?search=")
    ("rust-packages"    ?+  "crates.io/search?q=")
    ("softwareheritage" ?+  "archive.softwareheritage.org/browse/search/?q=")
    ("stackoverflow"    ?+  "stackoverflow.com/search?q=")
    ("twitter"          ?+  "twitter.com/search?q=")
    ("unicode-table"    ?+  "unicode-table.com/en/search/?q=")
    ("wikipedia-en"     ?_  "en.wikipedia.org/wiki/")
    ("wikipedia-pl"     ?_  "pl.wikipedia.org/wiki/")
    ("wolframalpha"     ?+  "wolframalpha.com/input/?i=")
    ("yandex"           ?+  "yandex.com/search/?text=")
    ("yewtube"          ?+  "yewtu.be/search?q=")  ; just a Invidious instance ;-)
    ("youtube"          ?+  "youtube.com/results?search_query="))
  "List of supported search engines.

Each element in this list is a list of three elements:
- name of the search engine, string,
  for example: \"duckduckgo\" or \"wikipedia-en\",
- separator used for queries, character,
  for example: \"+\" or \"_\", string,
- query URL (without \"https://\" prefix),
  for example: \"duckduckgo.com/?q=\" or \"en.wikipedia.org/wiki/\"."
  :type '(repeat (list string string string))
  :group 'search-engine)

(defcustom search-engine-custom-default-engine nil
  "Default search engine.

This is the default input for `completing-read' when prompted
to select an engine.
Can be set to any name of a search engine from ‘search-engine-custom-engines’."
  :type 'string
  :group 'search-engine)

(defcustom search-engine-custom-browse-url-function 'browse-url-default-browser
  "Function to browse a full query URL with."
  :type 'symbol
  :group 'search-engine)

(defcustom search-engine-custom-browse-url-function-candidates
  '(eww
    browse-url-default-browser
    browse-url-generic
    browse-url-firefox
    browse-url-chromium
    browse-url-chrome)
  "Candidates to browse a full query URL with."
  :type '(repeat symbol)
  :group 'search-engine)

(defcustom search-engine-custom-keymap-prefix "C-c C-s"
  "Keymap prefix for ‘search-engine-mode’."
  :type 'string
  :group 'search-engine)


(provide 'search-engine-custom)



;;; search-engine-custom.el ends here
