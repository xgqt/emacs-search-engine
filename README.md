# Emacs-Search-Engine


## About

Query search engines from Emacs.


## License

Copyright (c) 2022, Maciej Barć <xgqt@riseup.net>

Licensed under the GNU GPL v3 License

SPDX-License-Identifier: GPL-3.0-only
