;;; search-engine-mode.el --- Minor mode for search-engine -*- lexical-binding: t -*-


;; This file is part of emacs-search-engine.

;; emacs-search-engine is free software: you can redistribute it and/or modify
;; it under the terms of the GNU General Public License as published by
;; the Free Software Foundation, version 3.

;; emacs-search-engine is distributed in the hope that it will be useful,
;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;; GNU General Public License for more details.

;; You should have received a copy of the GNU General Public License
;; along with emacs-search-engine.  If not, see <https://www.gnu.org/licenses/>.

;; Copyright (c) 2022, Maciej Barć <xgqt@riseup.net>
;; Licensed under the GNU GPL v3 License



;;; Commentary:


;; Customization for ‘search-engine’.



;;; Code:


(require 'search-engine-custom)
(require 'search-engine)


(defun search-engine-mode--kbd (keys-string)
  "Return KEYS-STRING prefixed with ‘search-engine-custom-keymap-prefix’."
  (kbd (format "%s %s " search-engine-custom-keymap-prefix keys-string)))


;;;###autoload
(define-minor-mode search-engine-mode
  "Search engine minor mode."
  :global t
  :keymap
  (list (cons (search-engine-mode--kbd "b") #'search-engine-browse-with)
        (cons (search-engine-mode--kbd "p") #'search-engine-point)
        (cons (search-engine-mode--kbd "r") #'search-engine-region)
        (cons (search-engine-mode--kbd "k") #'search-engine-kill-ring)
        (cons (search-engine-mode--kbd "t") #'search-engine-term)
        (cons (search-engine-mode--kbd "s") #'search-engine))
  :group 'search-engine)

(define-derived-mode search-engine-engines-mode tabulated-list-mode
  "Search Engine Engines"
  "Major mode for listing ‘search-engine-custom-engines’."
  (setq tabulated-list-format
        [("Name"      20 t)
         ("Separator" 10 t)
         ("Query URL" 60 t)])
  (setq tabulated-list-sort-key (cons "Name" nil))
  (setq tabulated-list-entries
        (let ((index 0))
          (mapcar (lambda (entry)
                    (setq index (+ index 1))
                    (list index
                          (vector (nth 0 entry)
                                  (string (nth 1 entry))
                                  (nth 2 entry))))
                  search-engine-custom-engines)))
  (tabulated-list-init-header)
  (tabulated-list-print t))

;;;###autoload
(defun search-engine-list-engines ()
  "Display a list of supported search engines."
  (interactive)
  (let ((buffer (get-buffer-create "*Search Engine Engines*")))
    (with-current-buffer buffer
      (search-engine-engines-mode))
    (switch-to-buffer buffer)))


(provide 'search-engine-mode)



;;; search-engine-mode.el ends here
